#! /usr/local/env python3
import numpy as np
import os
import RNA
import random
from random import choice as sample
from copy import deepcopy
try:
    from cell_assembly.get_RNA_structures import fold_lib, fold_seq
except:
    from get_RNA_structures import fold_lib, fold_seq
#######################
## general file functions

def process_dir(in_dir):
    ## process the output dir
    if in_dir[-1]!='/':
        in_dir+='/'
    if not os.path.isdir(in_dir):
        os.makedirs(in_dir)
    return(in_dir)


def make_file(contents, path):
    file_handle = open(path, 'w')
    if isinstance(contents, list):
        file_handle.writelines(contents)
    elif isinstance(contents, str):
        file_handle.write(contents)
    file_handle.close()
    return()


def flatten_2D_table(table, delim):
    # print(type(table))
    if str(type(table)) == "<class 'numpy.ndarray'>":
        out = []
        for i in range(0, len(table)):
            out.append([])
            for j in range(0, len(table[i])):
                try:
                    str(table[i][j])
                except:
                    print(table[i][j])
                else:
                    out[i].append(str(table[i][j]))
            out[i] = delim.join(out[i]) + '\n'
        return out
    else:
        for i in range(0, len(table)):
            for j in range(0, len(table[i])):
                try:
                    str(table[i][j])
                except:
                    print(table[i][j])
                else:
                    table[i][j] = str(table[i][j])
            table[i] = delim.join(table[i]) + '\n'
        # print(table[0])
        return table


def write_table(table, out_file, sep='\t'):
    make_file(flatten_2D_table(table, sep), out_file)
    return()



def read_file(temp_file, lines_o_raw='lines', quiet=False):
    """ basic function library """
    lines = None
    if not quiet:
        print('reading', temp_file)
    file_handle = open(temp_file, 'r')
    if lines_o_raw == 'lines':
        lines = file_handle.readlines()
        for i, line in enumerate(lines):
            lines[i] = line.strip('\n')
    elif lines_o_raw == 'raw':
        lines = file_handle.read()
    file_handle.close()
    return(lines)


def make_table(lines, delim, range_min=0):
    for i in range(range_min, len(lines)):
        lines[i] = lines[i].strip()
        lines[i] = lines[i].split(delim)
        for j in range(range_min, len(lines[i])):
            if i!=0 or j!=0:
                try:
                    float(lines[i][j])
                except ValueError:
                    lines[i][j] = lines[i][j].replace('"', '')
                else:
                    lines[i][j] = float(lines[i][j])
    return lines


def read_table(file, sep='\t'):
    return make_table(read_file(file, 'lines'), sep)

######################
## general nucleotide functions
def rna_to_dna(rna):
    return(rna.upper().replace("U","T"))


def dna_to_rna(dna):
    return(dna.upper().replace("T","U"))


def rev_comp(seq):
    comp_dict = {"A":"T", 
                 "T":"A",
                 "U":"A",
                 "G":"C",
                 "C":"G"}
    rev_seq = list(seq[::-1])
    for i in range(len(rev_seq)):
        rev_seq[i]=comp_dict[rev_seq[i]]
    return("".join(rev_seq))


def transvert(letter):
    #Pur AG
    transversion_dict = {"A":"C",
                         "T":"G",
                         "G":"T",
                         "C":"A"}
    return(transversion_dict[letter])


###################################################################################
## get final sequences

def random_seq_to_final_seq(in_library, static_seq_list, num_concats = 25, num_random = 3):
    print("\tcompiling sequences")
    #return(merge_sgRNA_random_ms2(in_library))
    return(merge_sgRNA_random_static_seq_list(in_library, 
                                              static_seq_list = static_seq_list, 
                                              num_concats = num_concats, 
                                              num_random = num_random))


def get_concatenated_random_and_static_seq_list(in_seq, 
                                                static_seq_list, 
                                                num_concats, 
                                                num_random = 3):
    all_seqs = []
    ## first add a tandem array of all sequences on their own
    for temp_static in static_seq_list:
        all_seqs.append(in_seq + ( (temp_static + in_seq) * num_concats ))
    ## then add random sequences
    for n in range(num_random):
        final_seq = in_seq
        for i in range(num_concats):
            #print(final_seq)
            temp_selected_seq = sample(static_seq_list)
            final_seq += temp_selected_seq + in_seq
        all_seqs.append(final_seq)
    return(all_seqs)


def merge_sgRNA_random_static_seq_list(in_library, static_seq_list, num_concats=25, num_random = 3):
    """
    https://med.stanford.edu/qilab/resource.html
    https://www.cell.com/fulltext/S0092-8674(13)01531-6
    5'- GN19 # GTTTAAGAGCTATGCTGGA (insert new seq) AACAGCATAGCAAGTTTAAATAAGGCTAGTCCGTTATCAACTTGAAAAAGTGGCACCGAGTCGGTGCTTTTTTT-3’
    """
    five_prime_sgRNA = "GUUUAAGAGCUAUGCUGGA" ## this comes right after the G-N19 seq for targeting
    three_prime_sgRNA = "AACAGCAUAGCAAGUUUAAAUAAGGCUAGUCCGUUAUCAACUUGAAAAAGUGGCACCGAGUCGGUGCUUUUUUU"
    final_seqs = []
    for i in range(len(in_library)):
        final_seqs.append([])
        temp_center_seqs = get_concatenated_random_and_static_seq_list(in_library[i], 
                                                                       static_seq_list, 
                                                                       num_concats=num_concats,
                                                                       num_random = num_random)
        for temp_seq in temp_center_seqs:
            final_seqs[-1].append(five_prime_sgRNA+temp_seq+three_prime_sgRNA)
    return(final_seqs)

###################################################################################
## expand this toolkit to work with ms2, pp7, BoxB, and Com

###################################################################################

def seq_struct_to_merge_str(seq, struct):
    if len(seq)!= len(struct):
        sys.exit("sequence and structure are not the same length")
    out_seq_fold = ""
    for i in range(len(seq)):
        out_seq_fold += seq[i]+struct[i]
    return(out_seq_fold)


##############
## initialization steps
def get_random_seq(length, seq_type="RNA"):
    if seq_type=="RNA":
        letters = ["A","U","G","C"]
    elif seq_type=="DNA":
        letters = ["A","T","G","C"]
    out_seq = ""
    for i in range(length):
        out_seq+=np.random.choice(letters)
    return(out_seq)


def get_initial_seqs(num_seq, seq_len, seq_type = "RNA"):
    num_seq = int(num_seq)
    library = []
    for i in range(num_seq):
        library.append(get_random_seq(seq_len, seq_type = seq_type))
    return(library)


##############
## eval round

def eval_lib(library, lib_folds, desired_sequence_folds = ["A(C(A(U(G(A.G(G(A.U.C.A.C)C)C)A)U)G)U)"]):
    print("\tscoring sequences")
    required_sequence_folds = []## was going to make this the properly folded sgRNA structure, but turns out with WT seq, this isn't even the lowest energy level.. So...
    seq_to_avoid = ["UUUU"]
    ## lib folds is a list of lists 
    all_scores = np.zeros((len(library),len(library[0])))
    for i in range(len(lib_folds)):
        for j in range(len(lib_folds[i])):
            temp_lib_seq_struct = seq_struct_to_merge_str(library[i][j],lib_folds[i][j])
            temp_score = 0
            #print(temp_lib_seq_struct)
            #print(library[i])
            for desired in desired_sequence_folds:
                temp_desired_count = temp_lib_seq_struct.count(desired)
                #print("\t",desired,temp_desired_count,temp_desired_count)
                temp_score += temp_desired_count
                #print("\t\t\t",temp_score, temp_lib_seq_struct)
            if len(required_sequence_folds)>0:
                for required in required_sequence_folds:
                    temp_score *= bool(temp_lib_seq_struct.count(required)>0)
            if len(seq_to_avoid)>0:
                for cannot in seq_to_avoid:
                    no_cannot = int(not bool(temp_lib_seq_struct.count(cannot)>0))
                    temp_score *= no_cannot
                    if not no_cannot:
                        print("found banned seq ("+cannot+") in:",library[i][j])
            ## we'll also ban Us at the front or back. Just a precautionary measure to make sure that 
            ## we don't end up getting UUUU from concatenating with one of the static sequences
            # if library[i][0]=="U" or library[i][-1]=="U":
            #     temp_score = 0
            all_scores[i,j] = temp_score
    #print("\t\tmax:",np.max(all_scores),all_scores)
    print("all scores:")
    print(all_scores)
    return(all_scores)


#############
## evolve round
def mutate_seq(seq, mut_rate, seq_type="RNA"):
    if seq_type=="RNA":
        letters = ["A","U","G","C"]
    elif seq_type=="DNA":
        letters = ["A","T","G","C"]
    mut_prob = np.random.uniform(size=len(seq))
    mut_indices = np.where(mut_prob<mut_rate)[0]
    seq = list(seq)
    for idx in mut_indices:
        seq[idx]=np.random.choice(letters)
    return("".join(seq))


def evolve_lib(library, scores, survival_percent, mut_rate, seq_type = "RNA", fresh_rate = 0.2):
    keep_top_n = int(round(scores.shape[0]*survival_percent,0))
    if np.max(scores)==0:
        print("\t\tThis library was crap - starting from scratch")
        return(get_initial_seqs(num_seq = len(library), seq_len = len(library[0]), seq_type = seq_type))
    passing_indices = np.argsort(scores)[::-1][:keep_top_n]
    passing_indices = passing_indices[np.where(scores[passing_indices]>0)]
    passing_sequences = np.array(library)[passing_indices].tolist()
    print("\tevolving top",len(passing_indices),"sequences")
    print(passing_indices)
    parent_sequences = deepcopy(passing_sequences)
    parent_scores = deepcopy(scores[passing_indices])
    print("parent_sequences:")
    for parent in range(len(parent_sequences)):
        temp_parent = parent_sequences[parent]
        temp_score = parent_scores[parent]
        print("\t",temp_parent,"-",temp_score)
    ## insert fresh blood
    #print(passing_sequences)
    ## add ~20% new random sequences
    fresh_blood = get_initial_seqs(num_seq = int(fresh_rate * len(library)),
                                   seq_len = len(library[0]))
    passing_sequences += fresh_blood
    #print(passing_sequences)
    ## evolve the old seqs
    temp_parent = 0
    while len(passing_sequences) < len(library):
        #print(temp_parent, parent_sequences[temp_parent])
        #passing_sequences.append(parent_sequences[temp_parent])
        temp_new_seq = mutate_seq(parent_sequences[temp_parent], mut_rate = mut_rate)
        if temp_new_seq not in passing_sequences:
            passing_sequences.append(temp_new_seq)
        ## go to the next parent (or start over if we've made it to the end of the parents)
        temp_parent += 1
        if temp_parent >= len(parent_sequences):
            temp_parent = 0
        ## just in case, by chance, there were child sequences that didn't diverge from their parent
        passing_sequences = list(set(passing_sequences))
    return(passing_sequences)

## 
def sort_by_scores(scores, library, compiled_seqs, full_scores):
    sorted_indices = np.argsort(scores)[::-1]
    library = np.array(library)[sorted_indices].tolist()
    compiled_seqs = np.array(compiled_seqs)[sorted_indices].tolist()
    scores = scores[sorted_indices]
    full_scores = full_scores[sorted_indices,:]
    return(scores, library, compiled_seqs, full_scores)


#############
## do final evalulation
def do_final_evaluation(library, 
                        compiled_seqs, 
                        lib_folds, 
                        scores, 
                        full_scores,
                        desired_sequence_folds,
                        num_concats,
                        num_random = 25,
                        top_n = 25,
                        multiple = 2):
    scores, library, compiled_seqs, full_scores = sort_by_scores(scores, 
                                                                 library, 
                                                                 compiled_seqs, 
                                                                 full_scores)
    scores = scores[:top_n]
    library = library[:top_n]
    compiled_seqs = compiled_seqs[:top_n]
    full_scores = full_scores[:top_n]
    #print("scores",scores)
    #print("library",library)
    #print("full_scores",full_scores)
    compiled_seqs = random_seq_to_final_seq(library, 
                                            static_seq_list = static_seq_list, 
                                            num_concats = int(num_concats*multiple),
                                            num_random = num_random)
    #print(compiled_seqs)
    lib_folds = fold_lib(compiled_seqs)
    full_scores = eval_lib(compiled_seqs, 
                          lib_folds, 
                          desired_sequence_folds = desired_sequence_folds)/(num_concats*multiple)
    scores = np.min(full_scores, axis = 1)
    scores, library, compiled_seqs, full_scores = sort_by_scores(scores, 
                                                                 library, 
                                                                 compiled_seqs, 
                                                                 full_scores)
    print("\n\nFinal evaluation result:\n")
    print(full_scores)
    print(scores)
    return(library, 
           compiled_seqs, 
           lib_folds, 
           scores, 
           full_scores)

#############
## do the full evolution

def run_evolution(num_seq = 1000, 
                  seq_len = 18, 
                  survival_percent=0.2, 
                  mut_rate=0.10, 
                  num_rounds=50, 
                  seq_type="RNA",
                  num_concats = 25,
                  static_seq_list = ["acatgaggatcacccatgt"]):## default is only ms2
    ## first make sure the static seq list is in the right format
    if seq_type == "RNA":
        convert_func = dna_to_rna
    else:
        convert_func = rna_to_dna
    for i in range(len(static_seq_list)):
        static_seq_list[i] = convert_func(static_seq_list[i])
    ## fold the static sequences so that we know what structures we'll need to have
    desired_sequence_folds = []
    for i in range(len(static_seq_list)):
        desired_sequence_folds.append(seq_struct_to_merge_str(static_seq_list[i], fold_seq(static_seq_list[i])))
        print("static seq:",static_seq_list[i])
        print(desired_sequence_folds[i])
    ## now initialize the sequences
    library = get_initial_seqs(num_seq = num_seq, seq_len = seq_len, seq_type = seq_type)
    for evo_round in range(num_rounds):
        print("\nEvolutionary round #:"+str(evo_round))
        compiled_seqs = random_seq_to_final_seq(library, 
                                                static_seq_list = static_seq_list, 
                                                num_concats = min([num_concats, max([evo_round,5])]))
        lib_folds = fold_lib(compiled_seqs)
        full_scores = eval_lib(compiled_seqs, 
                              lib_folds, 
                              desired_sequence_folds = desired_sequence_folds)
        scores = np.min(full_scores, axis = 1)
        print(scores)
        if evo_round < (num_rounds-1):
            library = evolve_lib(library, 
                                 scores, 
                                 survival_percent = survival_percent, 
                                 mut_rate = mut_rate,
                                 seq_type = seq_type)
    ###########
    ## sort them so that the top n are the best ones
    print("\n######################################\nrunning final evaulation\n######################################\n")
    library, compiled_seqs, lib_folds, scores, full_scores = do_final_evaluation(library, 
                                                                                 compiled_seqs, 
                                                                                 lib_folds, 
                                                                                 scores, 
                                                                                 full_scores,
                                                                                 desired_sequence_folds,
                                                                                 num_concats = num_concats)
    return(library, compiled_seqs, lib_folds, scores, full_scores)

##################################################################
## thorough barage of tests

##################################################################
#################################################

## functions for writing the output
def write_folded_images(library,
                        scores,
                        compiled_seqs,
                        lib_folds,
                        out_dir,
                        static_seq_names,
                        static_seq_list,
                        num = 20):
    #out_dir = process_dir(os.path.join(out_dir,"folded_examples"))
    for i in range(num):
        linker = rna_to_dna(library[i])
        score = scores[i]
        ## set up the out directory
        out_name = "linker_"+str(i)+"_"+linker+"_score_"+str(np.round(score,2))
        out_short_name = "linker_"+str(i)
        #out_dir = process_dir(os.path.join(out_dir, "oligo_orders"))
        temp_out_dir = process_dir(os.path.join(out_dir,out_name,"folded_examples"))
        out_fasta = []
        for j in range(len(compiled_seqs[i])):
            if j < len(static_seq_names):
                print(compiled_seqs[i][j],lib_folds[i][j])
                base_name = 'linker_'+str(i)+"_"+library[i]+"_all_"+str(static_seq_names[j])
            else:
                base_name = 'linker_'+str(i)+"_"+library[i]+"_possibleSeqNum_random_"+str(j-len(static_seq_names))
            out_file = os.path.join(temp_out_dir, base_name) + '.svg'
            print(base_name)
            out_fasta.append([">"+base_name])
            out_fasta.append([compiled_seqs[i][j]])
            out_fasta.append([lib_folds[i][j]])
            if j-len(static_seq_names) < 0:
                RNA.svg_rna_plot(compiled_seqs[i][j],
                                 lib_folds[i][j], 
                                 out_file)
        write_table(out_fasta, os.path.join(temp_out_dir, 'linker_'+str(i)+"_folds.fasta"))
    return()


def write_full_scores(library,
                      compiled_seqs,
                      static_seq_names,
                      full_scores,
                      out_dir):
    #####################
    ## prep the header
    header = ["linker_number", "linker_sequence"]
    for temp_name in static_seq_names:
        header += ["all_"+temp_name]
    num_compiled_seqs = int(full_scores.shape[1] - len(static_seq_names))
    for num in range(num_compiled_seqs):
        header.append("compiled_"+str(num))
    ## finish the file
    output_full_scores_file = os.path.join(out_dir, "full_scores_of_last_round.tsv")
    out_full_scores = [header]
    for i in range(20):
        # for j in range(compiled_seqs[i]):
        # print(">compiled_seq_"+str(i)+"_"+library[i]+"_compiledSeq_"+str(j),"\n"+compiled_seqs[i][j])
        temp_line = [i, library[i]]
        out_full_scores.append(temp_line + full_scores[i].tolist())
    write_table(out_full_scores, output_full_scores_file)
    return()


def write_oligo_orders(static_seq_list,
                       static_seq_names,
                       library,
                       scores,
                       out_dir,
                       num = 20,
                       three_prime_sgRNA = "AACAGCAUAGCAAGUUUAAAUAAGGCUAGUCCGUUAUCAACUUGAAAAAGUGGCACCGAGUCGGUGCUUUUUUU"):
    ## make the reverse oligo the dna version of the same length as the linker
    three_prime_oligo = rna_to_dna(three_prime_sgRNA)[:len(library[0])]
    ## 
    for n in range(num):
        temp_output = []
        linker = rna_to_dna(library[n])
        ## the bridge and reverse are universal
        bridge = '/5Phos/' + rev_comp(linker + three_prime_oligo)
        rev = '/5Phos/' + rev_comp(three_prime_oligo)
        score = scores[n]
        ## set up the out directory
        out_name = "linker_"+str(n)+"_"+linker+"_score_"+str(np.round(scores[n],2))
        out_short_name = "linker_"+str(n)
        #out_dir = process_dir(os.path.join(out_dir, "oligo_orders"))
        temp_out_dir = process_dir(os.path.join(out_dir,out_name,"oligo_orders"))
        print("\n",out_name)
        ## get the static sequence specific oligos
        for s in range(len(static_seq_list)):
            static_seq = rna_to_dna(static_seq_list[s])
            static_seq_name = static_seq_names[s]
            fwd = linker + static_seq + linker
            ##
            wt_block = rev_comp(linker + static_seq)+"/3InvdT/"
            ## for mutant, we take the transversion of the 3' end of the linker
            mut_link = list(linker)
            mut_link[-1] = transvert(mut_link[-1])
            mut_link = ''.join(mut_link)
            mBlock = rev_comp(mut_link + static_seq)+"/3InvdT/"
            fwd_line = [out_short_name+"_"+static_seq_name+"_Fwd", fwd, "100nm", "HPLC"]
            wt_block_line = [out_short_name+"_"+static_seq_name+"_WtBlock", wt_block, "25nm", "STD"]
            mBlock_line = [out_short_name+"_"+static_seq_name+"_mBlock", mBlock, "25nm", "STD"]
            temp_output.append(fwd_line)
            temp_output.append(wt_block_line)
            temp_output.append(mBlock_line)
            print(fwd_line)
            print(wt_block_line)
            print(mBlock_line)
        bridge_line = [out_short_name+"_"+static_seq_name+"_bridge", bridge, "100nm", "HPLC"]
        rev_line = [out_short_name+"_"+static_seq_name+"_Rev", rev, "25nm", "STD"]
        temp_output.append(bridge_line)
        temp_output.append(rev_line)
        print(bridge_line)
        print(rev_line)
        write_table(temp_output, os.path.join(temp_out_dir,"oligo_orders.tsv"))
    return()


def write_ReLiger_oligo_orders(static_seq_list,
                       static_seq_names,
                       library,
                       scores,
                       out_dir,
                       num = 20,
                       three_prime_sgRNA = "AACAGCAUAGCAAGUUUAAAUAAGGCUAGUCCGUUAUCAACUUGAAAAAGUGGCACCGAGUCGGUGCUUUUUUU",
                       five_prime_terminator_seq = ""):## 5' term could be T7 transcription start site for recombinant synth from scratch
    ## make the reverse oligo the dna version of the same length as the linker
    three_prime_extension_length = len(library[0])+6
    if five_prime_terminator_seq != "":
        three_prime_oligo = rna_to_dna(three_prime_sgRNA)
    else:
        three_prime_oligo = rna_to_dna(three_prime_sgRNA)[:three_prime_extension_length]
    ## 
    for n in range(min([num,len(library)])):
        ## overall design is 
        # p*linker:static_seq      p*linker : static_seq
        # ----InvTstatic_seqComp:linkerComp
        temp_output = []
        linker = rna_to_dna(library[n])
        ## the three prime terminator
        three_prime_connector = '/5Phos/' + linker + three_prime_oligo
        ###############################################
        ###############################################
        score = scores[n]
        ## set up the out directory
        out_name = "linker_"+str(n)+"_"+linker+"_score_"+str(np.round(scores[n],2))
        out_short_name = "linker_"+str(n)
        #out_dir = process_dir(os.path.join(out_dir, "oligo_orders"))
        temp_out_dir = process_dir(os.path.join(out_dir,out_name,"ReLiger_oligo_orders"))
        print("\n",out_name)
        ##############################################
        ## add the 5' sequences if necessary
        if five_prime_terminator_seq != "":
            five_term_seq = five_prime_terminator_seq
            temp_output.append([out_short_name+"_5primeTerm", five_term_seq, "25nm", "STD"])
            five_term_splint = rev_comp(five_prime_terminator_seq[-18:] + linker) +"/3InvdT/"
            ## IDT requires orders of 100nm or more for inverted T modification
            temp_output.append([out_short_name+"_5primeSplint", five_term_splint, "100nm", "STD"])
            five_prime_amplifier = five_prime_terminator_seq[:20]
            temp_output.append([out_short_name+"_5primeAmp", five_prime_amplifier, "25nm", "STD"])
            three_prime_amplifier = three_prime_sgRNA[-20:] + "/3BioTEG/"
        ############################################
        ## add the central repetitive element sequences
        ## get the static sequence specific oligos
        for s in range(len(static_seq_list)):
            static_seq = rna_to_dna(static_seq_list[s])
            static_seq_name = static_seq_names[s]
            ##
            temp_static_ligatable = '/5Phos/' + linker + static_seq
            temp_static_ligatable_name = "pLink_"+static_seq_name
            temp_output.append([out_short_name+"_"+temp_static_ligatable_name, temp_static_ligatable, "25nm", "STD"])
            ##
            temp_static_splint = rev_comp(static_seq + linker)+"/3InvdT/"
            temp_static_splint_name = static_seq_name+"_Link_splintT"
            ## IDT requires orders of 100nm or more for inverted T modification
            temp_output.append([out_short_name+"_"+temp_static_splint_name, temp_static_splint, "100nm", "STD"])
        ###############################################
        ## add the three prime sequences
        temp_output.append([out_short_name+"_3primeTerm", three_prime_connector, "25nm", "STD"])
        if five_prime_terminator_seq != "":
            temp_output.append([out_short_name+"_3primeAmp", three_prime_amplifier, "100nm", "HPLC"])
        ###############################################
        write_table(temp_output, os.path.join(temp_out_dir,"ReLiger_oligo_orders.tsv"))
    return()



def save_evolution_results(library, 
                           compiled_seqs, 
                           lib_folds, 
                           scores, 
                           full_scores, 
                           static_seq_list,
                           static_seq_names = ["MS2", "PP7", "BoxB", "Com"],
                           out_dir = "/home/scott/bin/cell_assembly/lib/RePCR_evolved_4seqs/"):
    out_dir = process_dir(out_dir)
    ## now write the results
    write_full_scores(library,
                      compiled_seqs,
                      static_seq_names,
                      full_scores,
                      out_dir)
    write_oligo_orders(static_seq_list,
                       static_seq_names,
                       library,
                       scores,
                       out_dir)
    write_folded_images(library,
                        scores,
                        compiled_seqs,
                        lib_folds,
                        out_dir,
                        static_seq_names,
                        static_seq_list)
    return()


##########################
if __name__ == "__main__":
    np.random.seed(123456)
    random.seed(123456)
    ms2 = "acatgaggatcacccatgt".upper().replace("T","U")
    pp7 = "aacataaggagtttatatggaaacccttatg".upper().replace("T","U")
    BoxB = "gggccctgaagaagggccc".upper().replace("T","U")
    Com = "cctgaatgcctgcgagcat".upper().replace("T","U")
    do_all_ms2 = False
    if do_all_ms2:
        library, compiled_seqs, lib_folds, scores, full_scores = run_evolution()
        for i in range(20):
            RNA.svg_rna_plot(compiled_seqs[i],lib_folds[i],'/home/scott/Downloads/test_delete/evo_'+str(i)+'.svg' )
    else:
        static_seq_list = [ms2, pp7, BoxB, Com]
        library, compiled_seqs, lib_folds, scores, full_scores = run_evolution(static_seq_list = static_seq_list)
        #library, compiled_seqs, lib_folds, scores, full_scores = run_evolution(static_seq_list = [ms2, pp7, BoxB])
        save_evolution_results(library, 
                               compiled_seqs, 
                               lib_folds, 
                               scores, 
                               full_scores,
                               static_seq_list,
                               out_dir = "/media/scott/ssd_2tb/RePCR/")
        

