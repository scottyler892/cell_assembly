#! /usr/local/env python3
import numpy as np
import os
import RNA
import random
from random import choice as sample
from copy import deepcopy
import argparse
try:
    from cell_assembly.get_RNA_structures import fold_lib, fold_seq
    from cell_assembly.evolve_sequences import process_dir, make_file, write_table, read_table
except:
    from get_RNA_structures import fold_lib, fold_seq
    from evolve_sequences import process_dir, make_file, write_table, read_table
#####################################################

def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument(
        '-in_file','-i','-fasta',
        type=str)
    parser.add_argument(
        '-out_dir',
        default = "",
        type=str)
    parser.add_argument(
        '-seq_numbers',
        help = "if you only want to plot a specific index rather than all of the sequences.",
        nargs = "+",
        type=int)
    args = parser.parse_args()
    return(args)


def process_fasta(in_table):
	out_seq_list = []
	temp_log = []
	for i in range(len(in_table)):
		if in_table[i][0][0]==">":
			## start a new log
			if i != 0:
				out_seq_list.append(deepcopy(temp_log))
			temp_log = [in_table[i][0]]
		else:
			temp_log.append(in_table[i][0])
	out_seq_list.append(temp_log)
	return(out_seq_list)


def plot_from_fasta(out_dir, in_seq_list):
	## input is expected to be a list with the first line as
	## the title line with carrot
	## the second line the sequence
	## the third line is optional, if present is the ..(()).. style structure
	if len(in_seq_list)==2:
		in_seq_list.append(fold_seq(in_seq_list[1]))
	out_file = os.path.join(out_dir, in_seq_list[0][1:]+".svg")
	RNA.svg_rna_plot(in_seq_list[1],
                     in_seq_list[2], 
                     out_file)
	return()


def plot_all_seqs(in_file, out_dir, seq_numbers = None):
	seq_lists = process_fasta(read_table(in_file))
	if seq_numbers == None:
		seq_numbers = list(range(len(seq_lists)))
	for seq_idx in seq_numbers:
		plot_from_fasta(out_dir, seq_lists[seq_idx])
	return()


if __name__ == "__main__":
	args = parse_args()
	plot_all_seqs(in_file, out_dir, seq_numbers = None)
