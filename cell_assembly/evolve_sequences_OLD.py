# -*- coding: utf-8 -*-
"""

"""

from __future__ import division, print_function, absolute_import

##import dependency libraries
import numpy as np
import tflearn
import tensorflow as tf
import sys,time,glob,os,pickle,gc
from subprocess import Popen
gc.enable()
from tflearn.layers.recurrent import bidirectional_rnn, BasicLSTMCell, GRUCell
##############################################################
## basic function library
def read_file(tempFile,linesOraw='lines',quiet=False):
    if not quiet:
        print('reading',tempFile)
    f=open(tempFile,'r')
    if linesOraw=='lines':
        lines=f.readlines()
        for i in range(0,len(lines)):
            lines[i]=lines[i].strip('\n')
    elif linesOraw=='raw':
        lines=f.read()
    f.close()
    return(lines)

def make_file(contents,path):
    path = path.replace('//','/')
    f=open(path,'w')
    if isinstance(contents,list):
        f.writelines(contents)
    elif isinstance(contents,str):
        f.write(contents)
    f.close()

    
def flatten_2D_table(table,delim):
    #print(type(table))
    if str(type(table))=="<class 'numpy.ndarray'>":
        out=[]
        for i in range(0,len(table)):
            out.append([])
            for j in range(0,len(table[i])):
                try:
                    str(table[i][j])
                except:
                    print(table[i][j])
                else:
                    out[i].append(str(table[i][j]))
            out[i]=delim.join(out[i])+'\n'
        return(out)
    else:
        for i in range(0,len(table)):
            for j in range(0,len(table[i])):
                try:
                    str(table[i][j])
                except:
                    print(table[i][j])
                else:
                    table[i][j]=str(table[i][j])
            table[i]=delim.join(table[i])+'\n'
    #print(table[0])
        return(table)

def strip_split(line, delim = '\t'):
    return(line.strip('\n').split(delim))

def make_table(lines,delim):
    for i in range(0,len(lines)):
        lines[i]=lines[i].strip()
        lines[i]=lines[i].split(delim)
        for j in range(0,len(lines[i])):
            try:
                float(lines[i][j])
            except:
                lines[i][j]=lines[i][j].replace('"','')
            else:
                lines[i][j]=float(lines[i][j])
    return(lines)


def read_table(file, sep='\t'):
    return(make_table(read_file(file,'lines'),sep))
    
def write_table(table, out_file, sep = '\t'):
    make_file(flatten_2D_table(table,sep), out_file)
    

def import_dict(f):
    f=open(f,'rb')
    d=pickle.load(f)
    f.close()
    return(d)

def save_dict(d,path):
    f=open(path,'wb')
    pickle.dump(d,f)
    f.close()

def cmd(in_message, com=True):
    print(in_message)
    time.sleep(.25)
    if com:
        Popen(in_message,shell=True).communicate()
    else:
        Popen(in_message,shell=True)



##############################################################
import argparse
parser = argparse.ArgumentParser()

parser.add_argument(
    "-model", 
    help="model saved after training a tensorflow neural network")
parser.add_argument(
    "-network", "-net",
    help="the network text file corresponding to the pre-trained model",
    type = str)
parser.add_argument(
    "-out", "-o",
    help="the output directory",
    type = str)
parser.add_argument(
    "-mutation_rate", '-mut',
    help="what mutation rate should we use?",
    type= float,
    default = 0.05)
parser.add_argument(
    "-percent_survival", 
    help="what percent of each cohort will survive and be able to breed",
    type= float,
    default = 0.10)
parser.add_argument(
    "-crossover",
    help="probability of crossover at each base pair",
    type= float,
    default = 5e-3)
parser.add_argument(
    "-rounds",
    help="the number of evolutionary rounds",
    type= int,
    default = 500)
parser.add_argument(
    "-checkpoint",
    help="the interval of rounds to save the current sequences, and plot the results",
    type= int,
    default = 10)
parser.add_argument(
    "-pool_size",
    help="The original size of the pool to evolve from (per category)",
    type= int,
    default = 1000)
parser.add_argument(
    "-self_sim_penalty",
    help="How much should we penalize for self similarity?",
    type= float,
    default = 0.05)
parser.add_argument("-symbols","-nucleotides",'-nuc','-aa',
    help="typically this will be taken care of by the -seq_type argument, but if you have a non-typical fasta file, you can override the typical nucleotides or AAs used.",
    type = str)
parser.add_argument("-seq_type",
    help="DNA (d), RNA (r), or protein (p, prot)",
    type = str,
    default = 'DNA')
args = parser.parse_args()
###########################################################################
if not os.path.isdir(args.out):
    cmd('mkdir '+args.out)


###########################################################################

if args.symbols == None:
    if args.seq_type.upper() == 'DNA' or args.seq_type.upper() == 'D':
        args.symbols = 'ATGC'
    if args.seq_type.upper() == 'RNA' or args.seq_type.upper() == 'R':
        args.symbols = 'AUGC'
    if args.seq_type.upper() == 'protein' or args.seq_type.upper() == 'P' or args.seq_type.upper() == 'PROT':
        args.symbols = 'ACDEFGHIKLMNPQRSTVWY'

symbol_list = np.array(list(args.symbols))
## make the dictionary of which rows to put the letters from the sequence
args.symbols = args.symbols.upper()
letter_dict = {}
for i in range(0,len(args.symbols)):
    letter_dict[args.symbols[i]]=i

###########################################################################

def arrayify(in_str):
    global args
    ## first check that all of the strings that appear
    in_str = in_str.upper()
    temp_array = np.zeros((len(args.symbols), len(in_str)),dtype = int)
    for j in range(0,len(args.symbols)):
        indices = [i for i, x in enumerate(in_str) if x == args.symbols[j]]
        temp_array[j,indices]=1
    return(temp_array)

def array_to_seq(in_array):
    global symbol_list
    max_loci = np.argmax(in_array,axis=0).tolist()
    #print(max_loci)
    return(''.join(symbol_list[max_loci].tolist()))

def generate_random_seq_array(length):
    global args
    num_symbols = len(args.symbols)
    in_array = np.random.rand(num_symbols,length)
    max_loci = np.argmax(in_array,axis=0).tolist()
    seq_array = np.zeros((num_symbols,length))
    seq_array[max_loci,list(range(length))] = 1
    return(seq_array)

def mutate_array_seq(in_array):
    global args
    num_symbols = len(args.symbols)
    length = np.shape(in_array)[1]
    #args.mutation_rate
    mutation_array = np.random.rand(np.shape(in_array)[0],np.shape(in_array)[1])
    max_loci = np.argmax(mutation_array, axis = 0)
    ## mask all of the values (to zero) that were not the greatest in that column
    ## this will say (if the value is larger than the mutation rate), then turn it into
    ## the given symbol, which was the max for that column in the array
    masked_mut_array = np.zeros(np.shape(in_array))
    masked_mut_array[max_loci,list(range(length))] = mutation_array[max_loci,list(range(length))]
    ## boolify the mutations by whether or not the random value was greater than the
    ## mutation rate. We have to multiply by the number of symbols so that
    ## it will be the appropriate number of mutations.
    masked_mut_array = np.array(masked_mut_array > 1-(args.mutation_rate*num_symbols),dtype = float)
    ## merge the masked mutation array with the original sequence
    masked_mut_array = masked_mut_array+0.5
    in_array = in_array+masked_mut_array
    ## flatten the array to the max values, and make it binary again
    max_loci = np.argmax(in_array,axis=0).tolist()
    seq_array = np.zeros((num_symbols,length))
    seq_array[max_loci,list(range(length))] = 1
    return(seq_array)



###########################################################################

# if __name__ == "__main__":
#     starter_seq = 'gattacacatgcatgcatgcatgcagtca'
#     print(starter_seq)
#     in_array = arrayify(starter_seq)
#     for i in range(0,10):
#         print(array_to_seq(mutate_array_seq(in_array)))
#         print(array_to_seq(in_array))


###########################################################################



###########################################################################
## now we'll go through each category, assay the scores for each sequence 
## in that category

def score_layer(out_layer,max_cat):
    ## we'll start simple, and just calculate the ratio of the desired output to the total output
    score = np.log2(out_layer[max_cat]/(np.sum(out_layer)-out_layer[max_cat])+1)
    #score = out_layer[max_cat]
    ## in the future we may add a function to penalize self similarity
    return(score)


##########################################################################

def get_scores(evolving_sequences):
    global model
    score_array = np.zeros((len(evolving_sequences),args.pool_size))
    for i in range(0,len(evolving_sequences)):## each category
        predictions = np.array(model.predict(np.array(evolving_sequences[i])))
        #print(predictions)
        for j in range(0,args.pool_size):## each evolving sequence in the pool for that category
            score_array[i][j] = score_layer(predictions[j],i)
            # print(score_array[i][j])
            # print(type(evolving_sequences[0]))
            # print(np.shape(np.array(evolving_sequences)))
            # print(evolving_sequences[i,j])
            # score_array[i][j]=score_layer(model.predict(evolving_sequences[i][j]))
        #print(np.max(score_array[i]))
    return(score_array)

def get_mates(passing_seqs):
    # print(type(passing_seqs))
    rand_indices = np.random.shuffle(np.arange(len(passing_seqs)))
    return(passing_seqs[0],passing_seqs[1])



def crossover(mate1, mate2):
    global args
    mate1 = np.array(mate1)
    mate2 = np.array(mate2)
    child = mate1[:]
    #print(np.shape(child))
    indices = np.random.uniform(size = np.shape(child)[1])
    #print(indices)
    break_points = np.where(indices <= args.crossover)
    #print(break_points)
    break_points = break_points[0].tolist()
    break_points = [0]+break_points+[np.shape(child)[1]]
    break_points = sorted(list(set(break_points)))
    #print(break_points)
    for i in range(0,len(break_points)-1):
        j = i+1
        start = break_points[i]
        end = break_points[j]
        ## randomly assign this segment from one of the parents
        pick = np.random.uniform()
        if pick > 0.5:
            #print('crossing over',start,end)
            child[:,start:end] = mate2[:,start:end]
    #sys.exit()
    return(child)
    

def evolve(passing_list):
    global evolving_sequences
    evolving_sequences = np.zeros(np.shape(evolving_sequences))
    ## go through each of the groups
    for i in range(0,len(passing_list)):
        ## first add all of the parents to the returned evolving sequences
        evolving_sequences[i,list(range(len(passing_list[i])))]+=passing_list[i]
        num_children_needed = args.pool_size-len(passing_list[i])

        for j in range(len(passing_list[i]),args.pool_size):
            ## second pick out the mates
            temp_mate1, temp_mate2 = get_mates(passing_list[i])

            ## third, for each mating pair, do the crossovers
            child = crossover(temp_mate1,temp_mate2)

            ## lastly, mutate it
            child = mutate_array_seq(child)

            ## add this child to the next batch of evolving sequences
            evolving_sequences[i,j] += child
            

def multi_array_to_fasta(arrays,group_number):
    output_fasta_str = ''
    for i in range(0,len(arrays)):
        title = '>group_'+str(group_number)+'_seq_'+str(i)+'\n'
        fasta = array_to_seq(arrays[i])
        if i != len(arrays)-1:
            fasta+='\n'
        output_fasta_str+=title
        output_fasta_str+=fasta
    return(output_fasta_str)

def write_fastas(passing_list):
    global args
    for i in range(0,len(passing_list)):
        fasta_str = multi_array_to_fasta(passing_list[i],i)
        make_file(fasta_str, args.out+'/passing_seqs_'+str(i)+'.fasta')



import matplotlib.pyplot as plt
fig = plt.figure()
ax = fig.add_subplot(111)
def plot_scores(temp_scores, file,linestyle='-'):
    global fig, ax
    plt.clf()
    X = np.arange(len(temp_scores))
    #temp_colors = Set1(len(temp_scores[0]))
    temp_scores = np.array(temp_scores)
    for i in range(0,np.shape(temp_scores)[1]):
        plt.plot(X, temp_scores[:,i], 
            #color=temp_colors[i],
            linewidth=2.5, linestyle=linestyle,
            label = 'group_'+str(i))
    plt.legend(bbox_to_anchor=(1.05, 1), loc=2, borderaxespad=0.)
    ax.set_xlabel('evolutionary round')
    ax.set_ylabel('mean population score')
    plt.savefig(file,dpi=300,bbox_inches='tight')


def iterate_evolution(i):
    global evolving_sequences, mean_scores_by_round, all_upper_percentile_scores, args
    ## first score them
    temp_scores = get_scores(evolving_sequences)
    temp_means = np.mean(temp_scores, axis = 1)
    mean_scores_by_round.append(temp_means)
    print('\nmean scoring sequences in each group are:\n',temp_means)
    ## get the top fraction of sequences from each category
    upper_percentile_scores = np.percentile(temp_scores,100-(args.percent_survival*100), axis = 1)
    all_upper_percentile_scores.append(upper_percentile_scores)
    print('\ncutting off survival at scores of:\n',upper_percentile_scores)
    passing_list = []
    for j in range(0,len(evolving_sequences)):
        ## for each group which is in the highest specified percentile
        temp_passing_seq_arrays_indices = np.where(temp_scores[j,:] >= upper_percentile_scores[j])[0]
        #print(temp_passing_seq_arrays_indices)
        ## subset those sequences
        passing_list.append(np.array(evolving_sequences)[j][temp_passing_seq_arrays_indices].tolist())


    ## write the current leaders to fasta files
    if (i+1) % args.checkpoint == 0:
        write_fastas(passing_list)
        for j in range(0,len(passing_list)):
            make_file(str(passing_list[j]),args.out+'/passing_seqs_'+str(j)+'.array')
        plot_scores(np.array(mean_scores_by_round),args.out+'/mean_scores_by_round.png')
        plot_scores(np.array(all_upper_percentile_scores),args.out+'/lowest_surviving_scores_by_round.png',linestyle='--')

    ## now we need to evolve them
    print('\n\nevolving after round',i)
    evolve(passing_list)

##########################################################################




###########################################################################
## now we evolve the sequences to maximize the models

## first load the network and model
net_str = read_file(args.network,linesOraw='raw')
# print(net_str)
# print(type(net_str))
exec(net_str)


model = tflearn.DNN(net)
model.load(args.model)

###########################################################################
##### first we need to figure out the input and output dimentions
## in layer
sequence_length = 5000

## out layer
out_layer_length = 8

#### now we'll need to initialize the populations for each desired output
evolving_sequences = []
all_upper_percentile_scores = []
for i in range(0,out_layer_length):
    evolving_sequences.append([])
    for j in range(0,args.pool_size):
        evolving_sequences[i].append(generate_random_seq_array(sequence_length))

###########################################################################





mean_scores_by_round = []
## evolve them
if args.rounds != 0:# either for a set number of rounds
    for i in range(0,args.rounds):
        iterate_evolution(i)
else:## or continuously until the program is stopped
    i = 0
    while True:
        iterate_evolution(i)
        i+=1



