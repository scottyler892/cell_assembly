import numpy as np
import RNA
import ray
#######################

# sequence = "GUAUCGUAACAACGAUACAAUAAACAUGAGGAUCACCCAUGUGUAUCGUAACAACGAUACAAUAAACAUGAGGAUCACCCAUGUGUAUCGUAACAACGAUACAAUAAACAUGAGGAUCACCCAUGUGUAUCGUAACAACGAUACAAUAAACAUGAGGAUCACCCAUGUGUAUCGUAACAACGAUACAAUAAACAUGAGGAUCACCCAUGU"

# # Set global switch for unique ML decomposition

# RNA.cvar.uniq_ML = 1

# subopt_data = { 'counter' : 1, 'sequence' : sequence }


# Print a subopt result as FASTA record
def print_subopt_result(structure, energy, data):
    if not structure == None:
        print(">subopt %d" % data['counter'])
        print("%s" % data['sequence'])
        print("%s [%6.2f]" % (structure, energy))
        # increase structure counter
        data['counter'] = data['counter'] + 1


# # Create a 'fold_compound' for our sequence
# a = RNA.fold_compound(sequence)


# # Enumerate all structures 500 dacal/mol = 5 kcal/mol arround
# # the MFE and print each structure using the function above
# a.subopt_cb(500, print_subopt_result, subopt_data)

#####################################################



def store_structure(s, data):
    if s:
        data.append(s)

# """
# First we prepare a fold_compound object
# """
# # create model details
# md = RNA.md()
# # activate unique multibranch loop decomposition
# md.uniq_ML = 1
# # create fold compound object
# fc = RNA.fold_compound(sequence, md)
# # compute MFE
# (ss, mfe) = fc.mfe()
# # rescale Boltzmann factors according to MFE
# fc.exp_params_rescale(mfe)
# # compute partition function to fill DP matrices
# fc.pf()
# """
# Now we are ready to perform Boltzmann sampling
# """


# # 1. backtrace a single sub-structure of length 10
# print("%s" % fc.pbacktrack5(10))


# # 2. backtrace a single sub-structure of length 50
# print("%s" % fc.pbacktrack5(50))


# # 3. backtrace multiple sub-structures of length 10 at once
# for s in fc.pbacktrack5(20, 10):
#     print("%s" % s)


# # 4. backtrace multiple sub-structures of length 50 at once
# for s in fc.pbacktrack5(100, 50):
#     print("%s" % s)


# # 5. backtrace a single structure (full length)
# print("%s" % fc.pbacktrack())


# # 6. backtrace multiple structures at once
# for s in fc.pbacktrack(100):
#     print("%s" % s)


# # 7. backtrace multiple structures non-redundantly
# for s in fc.pbacktrack(100, RNA.PBACKTRACK_NON_REDUNDANT):
#     print("%s" % s)


# # 8. backtrace multiple structures non-redundantly (with resume option)
# num_samples = 500
# iterations  = 15
# d           = None # pbacktrack memory object
# s_list      = list()
# for i in range(0, iterations):
#     d, ss   = fc.pbacktrack(num_samples, d, RNA.PBACKTRACK_NON_REDUNDANT)
#     s_list  = s_list + list(ss)


# for s in s_list:
#     print("%s" % s)


# # 9. backtrace multiple sub-structures of length 50 in callback mode
# ss  = list()
# i   = fc.pbacktrack5(100, 50, store_structure, ss)
# for s in ss:
#     print("%s" % s)


# # 10. backtrace multiple full-length structures in callback mode
# ss  = list()
# i   = fc.pbacktrack(100, store_structure, ss)
# for s in ss:
#     print("%s" % s)


# # 11. non-redundantly backtrace multiple full-length structures in callback mode
# ss  = list()
# i   = fc.pbacktrack(100, store_structure, ss, RNA.PBACKTRACK_NON_REDUNDANT)
# for s in ss:
#     print("%s" % s)



# # 12. non-redundantly backtrace multiple full length structures


# # in callback mode with resume option

# ss = list()
# d  = None # pbacktrack memory object
# for i in range(0, iterations):
#     d, i = fc.pbacktrack(num_samples, store_structure, ss, d, RNA.PBACKTRACK_NON_REDUNDANT)


# for s in ss:
#     print("%s" % s)

#########################################################################
def get_indices(threads, num_genes):
    indices_list = []
    for t in range(threads):
        indices_list.append([])
    temp_idx = 0
    while temp_idx < num_genes:
        for t in range(threads):
            if temp_idx < num_genes:
                indices_list[t].append(temp_idx)
                temp_idx += 1
    return(indices_list)

def ray_get_contiguous_indices(threads, num_genes):
    old_format_indices = get_indices(threads, num_genes)
    new_indices = []
    for t in range(threads):
        new_indices.append([])
    cur_idx=0
    for t in range(threads):
        while len(new_indices[t]) <= len(old_format_indices[t]) and cur_idx<num_genes:
            new_indices[t].append(cur_idx)
            cur_idx+=1
    return(new_indices)


def fold_seq(seq):
    # create fold_compound data structure (required for all subsequently applied  algorithms)
    fc = RNA.fold_compound(seq)
    # compute MFE and MFE structure
    (mfe_struct, mfe) = fc.mfe()
    # rescale Boltzmann factors for partition function computation
    fc.exp_params_rescale(mfe)
    # compute partition function
    (pp, pf) = fc.pf()
    # compute centroid structure
    (centroid_struct, dist) = fc.centroid()
    # compute free energy of centroid structure
    centroid_en = fc.eval_structure(centroid_struct)
    # compute MEA structure
    (MEA_struct, MEA) = fc.MEA()
    # compute free energy of MEA structure
    MEA_en = fc.eval_structure(MEA_struct)
    # # print everything like RNAfold -p --MEA
    # print("%s\n%s (%6.2f)" % (seq, mfe_struct, mfe))
    # print("%s [%6.2f]" % (pp, pf))
    # print("%s {%6.2f d=%.2f}" % (centroid_struct, centroid_en, dist))
    # print("%s {%6.2f MEA=%.2f}" % (MEA_struct, MEA_en, MEA))
    # print(" frequency of mfe structure in ensemble %g; ensemble diversity %-6.2f" % (fc.pr_structure(mfe_struct), fc.mean_bp_distance()))
    return(MEA_struct)

def fold_seq(seq):
    # create fold_compound data structure (required for all subsequently applied  algorithms)
    fc = RNA.fold_compound(seq)
    # compute MFE and MFE structure
    (mfe_struct, mfe) = fc.mfe()
    # rescale Boltzmann factors for partition function computation
    fc.exp_params_rescale(mfe)
    # compute partition function
    (pp, pf) = fc.pf()
    # compute centroid structure
    (centroid_struct, dist) = fc.centroid()
    # compute free energy of centroid structure
    centroid_en = fc.eval_structure(centroid_struct)
    # compute MEA structure
    (MEA_struct, MEA) = fc.MEA()
    # compute free energy of MEA structure
    MEA_en = fc.eval_structure(MEA_struct)
    # # print everything like RNAfold -p --MEA
    # print("%s\n%s (%6.2f)" % (seq, mfe_struct, mfe))
    # print("%s [%6.2f]" % (pp, pf))
    # print("%s {%6.2f d=%.2f}" % (centroid_struct, centroid_en, dist))
    # print("%s {%6.2f MEA=%.2f}" % (MEA_struct, MEA_en, MEA))
    # print(" frequency of mfe structure in ensemble %g; ensemble diversity %-6.2f" % (fc.pr_structure(mfe_struct), fc.mean_bp_distance()))
    return(MEA_struct)


@ray.remote
def ray_fold_subset(indices, ray_seqs, do_print = False):
    temp_ray_out_structs = []
    for pos in range(len(indices)):
        i = indices[pos]
        if pos % 5 == 0 and do_print:
            print("\t\t",np.round(100*pos/len(indices)),"%")
        ## check if it's a list or sequence directly
        if type(ray_seqs[i]) is list:
            temp_ray_out_structs.append([])
            for j in range(len(ray_seqs[i])):
                temp_ray_out_structs[-1].append(fold_seq(ray_seqs[i][j]))
        else:
            temp_ray_out_structs.append(fold_seq(ray_seqs[i]))
    return(temp_ray_out_structs)


def fold_lib(seqs, processes = 11):
    print("\tfolding sequences")
    if processes == 1:
        out_folds = []
        for i in range(len(seqs)):
            if type(seqs[i]) is list:
                out_folds.append([])
                for j in range(len(seqs[i])):
                    out_folds[i].append(fold_seq(seqs[i][j]))
            else:
                if i%10 == 0 and i!=0:
                    print("\t\t",i)
                seq = seqs[i]
                out_folds.append(fold_seq(seq))
    else:
        ray.init()
        ray_calls = []
        ## add the ray calls
        temp_indices = ray_get_contiguous_indices(processes,len(seqs))
        ray_seqs = ray.put(seqs)
        for p in range(processes):
            if p == 0:
                ray_calls.append( ray_fold_subset.remote(temp_indices[p],
                                                         ray_seqs,
                                                         do_print = True))
            else:
                ray_calls.append( ray_fold_subset.remote(temp_indices[p],
                                                         ray_seqs,
                                                         do_print = False))
        ray_sults = ray.get(ray_calls)
        ray.shutdown()
        out_folds = []
        for temp_res in ray_sults:
            out_folds += temp_res
    return(out_folds)







# #################################################
# RNA.svg_rna_plot(seq,mfe_struct,'/home/scott/Downloads/test_delete/blah.svg' )
# RNA.svg_rna_plot(seq,mfe_struct,'/home/scott/Downloads/test_delete/blah2.svg' )

# RNA.xrna_plot(seq,mfe_struct,'/home/scott/Downloads/test_delete/blah.png' )


# ##########################


# import matplotlib.pyplot as plt
# import forgi.visual.mplotlib as fvm
# import forgi

# cg = forgi.load_rna("/home/scott/Downloads/test_delete/example.fx", allow_many=False)## allow many is for a list of input. Also returns a list of outputs
# fvm.plot_rna(cg, text_kwargs={"fontweight":"black"}, lighten=0.7,
#              backbone_kwargs={"linewidth":3})
# plt.show()